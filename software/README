#!/bin/sh

# low fuse:
# ======================================================================
# bit 7 CKDIV8 = 1 (don't divide clock by 8)
# bit 6 CKOUT = 1 (don't output clock on port b0)
# bit 5:4 SUT = 11 (part of "crystal with slow rise power")
# bit 3:0 CKSEL = 0111 (part of "crystal with slow rise power")
# Combined bits = 0xb11110111 = 0xf7
# ======================================================================
# 
# high fuse:
# ======================================================================
# bit 7 OCDEN = 1 (OCD disabled)
# bit 6 JTAGEN = 1 (JTAG disabled)
# bit 5 SPIEN = 0 (SPI programming enabled)
# bit 4 WDTON = 1 (watchdog disabled)
# bit 3 EESAVE = 1 (EEPROM not preserved by chip erase)
# bit 2:1 BOOTSZ[1:0] = 0b11 (size = 512 words, 1024 bytes)
# bit 0 BOOTRST = 0 (map reset vector to start of bootloader)
# Combined bits = 0b11011110 = 0xde
# ======================================================================
# 
# extended fuse:
# ======================================================================
# bits 7:3 = 0b11111 (unused)
# bits 2:0 = 0b101 (brown out detect for 2.7 volts)
# Combined bits = 0b11111101 = 0xfd
# ======================================================================
#
# Regarding optiboot, I'm using the falco branch:
cd ~/arduino/optiboot/optiboot/optiboot/bootloaders/optiboot
git checkout falco
make falco_sweep_generator
#
# Program the fuses:
avrdude -b 100000 -p m1284p -c C232HM -U lfuse:w:0xf7:m -U hfuse:w:0xde:m -U efuse:w:0xfd:m
#
# Program the bootloader:
P=/home/sfalco/arduino/optiboot/optiboot/optiboot/bootloaders/optiboot
avrdude -b 100000 -p m1284p -c C232HM -e -U flash:w:$P/optiboot_falco_sweep_generator.hex:i

# It looks like MightyCore would be a good BSP for the ATMEGA1284P.  I have
# a copy in ~/src/MightyCore.  The master copy is on github in
# https://github.com/MCUdude/MightyCore.  I've forked it and added a remote
# for it.
# 
# In order to get the C232HM recognized as a built-in programmer, I edited
# programmers.txt (in src/MightyCore/MightyCore/avr/programmers.txt) and
# added the entry.  The installed copy is instead here:
~/.arduino15/packages/MightyCore/hardware/avr/2.2.2/programmers.txt
# To get it to be recognized I had to blow away ~/.config/arduino-ide and
# restart the IDE.  That loses some configuration settings, which will have
# to be reentered.
#
# To do so, select Tools->Board->MightyCore->ATmega1284 and there should be
# a bunch of new menu items in the Tools menu; the C232HM should now be an
# available programmer.  This is currently untested, and I'll probably just
# use the upstream optiboot as described above, rather than going through the
# IDE.

# As to pin definitions, it looks like they are stored in:
~/.arduino15/packages/arduino/tools/avr-gcc/7.3.0-atmel3.6.1-arduino7/avr/include/avr
# There are files for each processor.  iom1284p.h is the one for our processor.

